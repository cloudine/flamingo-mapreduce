package org.openflamingo.mapreduce.core;

import org.apache.hadoop.conf.Configuration;
import org.openflamingo.mapreduce.util.JsonFormatter;
import org.openflamingo.mapreduce.util.StringUtils;

import java.io.StringWriter;
import java.util.Map;
import java.util.Set;

import static org.openflamingo.mapreduce.core.Constants.JOB_FAIL;

/**
 * Flamingo Workflow Engine과 MapReduce를 통합하기 위한 Workflow Job 인터페이스.
 * 이 인터페이스를 통해서 Workflow Engine은 Key Value 형식으로 파라미터를 MapReduce Job에 전달한다.
 *
 * @author Edward KIM
 * @author Seo Ji Hye
 * @since 0.1
 */
public abstract class WorkflowJob extends AbstractJob {

    /**
     * Workflow Engine 기반 환경에서 MapReduce를 실행한다.
     *
     * @param args Key Value 형식의 파라미터
     * @return 실행 결과
     * @throws Exception MapReduce 실행 에러 발생시
     */
    public abstract int execute(Map<String, String> args) throws Exception;

    /**
     * MapReduce에서 사용하는 옵션을 정의한다.
     *
     * @param args 커맨드 라인 파라미터터
     * @throws Exception 옵션 파싱 에러 발생시
     */
    public abstract void defineOptions(String[] args) throws Exception;

    @Override
    public int run(String[] args) throws Exception {
/*
        boolean isWorkflowEngine = false;
        for (String arg : args) {
            if (!StringUtils.isEmpty(arg) && "--engine".equals(arg)) {
                isWorkflowEngine = true;
                break;
            }
        }

        if (isWorkflowEngine) {
            addOption(buildOption("instanceId", null, "Workflow Instance ID", true, false, null));
            addOption(buildOption("workflowId", null, "Workflow ID", true, false, null));
            addOption(buildOption("jobId", null, "Job ID", true, false, null));
            addOption(buildOption("workflowName", null, "Workflow Name", true, false, null));
            addOption(buildOption("workflowHistoryId", null, "Workflow History ID", true, false, null));
            addOption(buildOption("actionHistoryId", null, "Action History ID", true, false, null));
            addOption(buildOption("actionDirectory", null, "Action Directory", true, false, null));
            addOption(buildOption("workflowDirectory", null, "Workflow Directory", true, false, null));
            addOption(buildOption("temporaryDirectory", null, "Temporary Directory", true, false, null));

            addOption("columnSize", null, "컬럼의 개수", true);
            addOption("columnNames", null, "컬럼의 이름", true);
            addOption("prevColumnNames", null, "이전 노드의 컬럼 이름", true);
            addOption("prevColumnSize", null, "이전 노드의 컬럼 개수", true);
        }
*/

        defineOptions(args);

        Map<String, String> parsedArgs = parseArguments(args);
        if (parsedArgs == null) {
            return JOB_FAIL;
        }

/*
        if (isWorkflowEngine) {
            this.getConf().set("instanceId", parsedArgs.get("--instanceId"));
            this.getConf().set("workflowId", parsedArgs.get("--workflowId"));
            this.getConf().set("jobId", parsedArgs.get("--jobId"));
            this.getConf().set("workflowName", parsedArgs.get("--workflowName"));
            this.getConf().set("workflowHistoryId", parsedArgs.get("--workflowHistoryId"));
            this.getConf().set("actionHistoryId", parsedArgs.get("--actionHistoryId"));
            this.getConf().set("columnsNames", parsedArgs.get("--columnsNames"));
            this.getConf().set("prevColumnNames", parsedArgs.get("--prevColumnNames"));
            this.getConf().set("columnSize", parsedArgs.get("--columnSize"));
            this.getConf().set("prevColumnSize", parsedArgs.get("--prevColumnSize"));
            this.getConf().set("actionDirectory", parsedArgs.get("--actionDirectory"));
            this.getConf().set("workflowDirectory", parsedArgs.get("--workflowDirectory"));
            this.getConf().set("temporaryDirectory", parsedArgs.get("--temporaryDirectory"));
        }
*/

        if (!StringUtils.isEmpty(parsedArgs.get("--inputDelimiter"))) this.getConf().set("inputDelimiter", parsedArgs.get("--inputDelimiter"));
        if (!StringUtils.isEmpty(parsedArgs.get("--outputDelimiter"))) this.getConf().set("outputDelimiter", parsedArgs.get("--outputDelimiter"));

        if (parsedArgs.containsKey("--verbose")) {

            System.out.println("\n\n");
            System.out.println("--------------------------------------------");
            System.out.println(" Hadoop Configuration");
            System.out.println("--------------------------------------------");

            Configuration conf = getConf();
            StringWriter writer = new StringWriter();
            conf.dumpConfiguration(conf, writer);
            String formattedJson = JsonFormatter.format(writer.toString());
            System.out.println(formattedJson);

            System.out.println("\n\n");
            System.out.println("--------------------------------------------");
            System.out.println(" System Environment");
            System.out.println("--------------------------------------------");

            Map<String, String> envs = System.getenv();
            Set<String> keys = envs.keySet();
            for (String key : keys) {
                String value = envs.get(key);
                System.out.println(key + "=" + value);
            }

            System.out.println("\n\n");
            System.out.println("--------------------------------------------");
            System.out.println(" System Properties");
            System.out.println("--------------------------------------------");

            Set<Object> propsKeys = System.getProperties().keySet();
            for (Object propsKey : propsKeys) {
                String key = (String) propsKey;
                String value = System.getProperty(key);
                System.out.println(key + "=" + value);
            }
            System.out.println("\n\n");
        }

        return execute(parsedArgs);
    }
}
