package org.openflamingo.mapreduce.core;

import org.apache.pig.PigServer;
import org.openflamingo.mapreduce.util.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Apache Pig의 기능을 다른 MapReduce Driver와 같이 동작하도록 공통적인 기능을 정의한 Pig Driver.
 *
 * @author Edward KIM
 * @author Seo Ji Hye
 * @since 0.1
 */
public class PigDriver {

	public static final String PARAM_JARS = "jars";
	public static final String PARAM_INPUT = "input";
	public static final String PARAM_OUTPUT = "output";
	public static final String PARAM_STORAGE = "PigStorage(\"{}\")";
	public static final String PARAM_DEFAULT_STORAGE = "PigStorage(',')";
	public static final String PARAM_COLUMN = "{}:{}";
	public static final String PQL_LOAD = "{} = LOAD {} USING {};";
	public static final String PQL_LOAD_AS = "{} = LOAD {} USING {} AS ({});";
	public static final String PQL_STORE = "STORE {} INTO {} USING {};";
	public static final String PQL_DUMP = "DUMP {};";
	public static final String[] RELATIONS = new String[]{
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
		"K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
		"U", "V", "W", "X", "Y", "Z"
	};

	/**
	 * Key Value Style Parameter Map
	 */
	private Map<String, String> params = null;

	/**
	 * Apache Pig Server
	 */
	private PigServer pigServer;

	/**
	 * Default Constructore.
	 *
	 * @param args Command Line Arguments
	 */
	public PigDriver(String[] args, PigServer pigServer) throws IOException {
		this.pigServer = pigServer;
		this.params = parseArguements(args);
		registExternalJar();
	}

	/**
	 * 공백으로 구분되어 있는 파라미터를 Key Value로 구성한다.
	 *
	 * @param args 커맨드라인 파라미터
	 * @return Key Value
	 */
	public static Map<String, String> parseArguements(String[] args) {
		// TODO Commons-CLI
		Map<String, String> params = new HashMap<String, String>();
		for (int i = 0; i < args.length; ++i) {
			params.put(args[i], args[++i]);
		}
		return params;
	}

	/**
	 * Key Value Style Parameter Map을 반환한다.
	 *
	 * @return Key Value Style Parameter Map
	 */
	public Map<String, String> getParams() {
		return params;
	}

	/**
	 * External JAR 파일을 Pig에 등록한다.
	 *
	 * @throws java.io.IOException JAR 파일이 존재하지 않는 경우
	 */
	public void registExternalJar() throws IOException {
		String[] strings = getCommaDelimitedParameter(PARAM_JARS);
		if (strings != null) {
			for (String path : strings) {
				registExternalJar(pigServer, path);
			}
		}
	}

	/**
	 * External JAR 파일을 Pig에 등록한다.
	 *
	 * @param pigServer Pig Server
	 * @param jar       JAR 파일 경로
	 * @throws java.io.IOException JAR 파일이 존재하지 않는 경우
	 */
	public void registExternalJar(PigServer pigServer, String jar) throws IOException {
		pigServer.registerJar(jar);
	}

	/**
	 * 지정한 Key에 대한 파라미터를 배열로 반환한다.
	 *
	 * @param key 파라미터 Key
	 * @return 파라미터 Key에 대한 문자열 배열
	 */
	public String[] getCommaDelimitedParameter(String key) {
		String commaDelimitedJars = params.get(key);
		return StringUtils.commaDelimitedListToStringArray(commaDelimitedJars);
	}

	/**
	 * Pig Server를 반환한다.
	 *
	 * @return Pig Server
	 */
	public PigServer getPigServer() {
		return pigServer;
	}
}