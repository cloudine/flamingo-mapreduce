package org.openflamingo.mapreduce.etl.sort;

import org.apache.pig.PigServer;
import org.openflamingo.mapreduce.core.Constants;
import org.openflamingo.mapreduce.core.PigDriver;
import org.slf4j.helpers.MessageFormatter;

import java.io.IOException;
import java.text.MessageFormat;

/**
 * 지정한 컬럼을 기준으로 정렬하는 Sort Driver.
 *
 * @author Edward KIM
 * @author Seo Ji Hye
 * @since 0.1
 */
public class SortDriver extends PigDriver {

	/**
	 * Pig Order By Phase
	 */
	public static final String PQL_SORT = "{} = ORDER {} BY {};";

	/**
	 * Default Constructore.
	 *
	 * @param args Command Line Arguments
	 */
	public SortDriver(String[] args, PigServer pigServer) throws IOException {
		super(args, pigServer);
	}

	public void main(String[] args) throws Exception {
		PigServer pigServer = new PigServer("mapreduce");
		SortDriver driver = new SortDriver(args, pigServer);
		driver.load("A");
		driver.registerQuery("B", "A");
		driver.store("B");
		System.exit(0);
	}

	public void load(String relation) throws IOException {
		String delimiter = getParams().get(Constants.DEFAULT_DELIMITER);
		String pql = MessageFormat.format(PQL_LOAD, relation, getParams().get("input"), PARAM_DEFAULT_STORAGE);
		getPigServer().registerQuery(pql);
	}

	public void store(String relation) throws IOException {
		String delimiter = getParams().get(Constants.DEFAULT_DELIMITER);
		String pql = MessageFormat.format(PQL_STORE, relation, getParams().get("output"), PARAM_DEFAULT_STORAGE);
		getPigServer().registerQuery(pql);
	}

	public void registerQuery(String relation, String sourceRelation) throws IOException {
		String[] columns = this.getCommaDelimitedParameter("columns");
		String[] orders = this.getCommaDelimitedParameter("orders");
		OrderBuilder builder = new OrderBuilder();
		for (int i = 0; i < columns.length; i++) {
			String column = columns[i];
			String order = orders[i];
			builder.addOrder(new Order(column, order));
		}
		String condition = builder.toString();
		String pql = MessageFormatter.arrayFormat(PQL_SORT, new String[]{relation, sourceRelation, condition}).getMessage();
		getPigServer().registerQuery(pql);
	}

	/**
	 * Order By
	 */
	public static class Order {

		private String column;

		private String order;

		public static final String PQL_SORT_ORDER = "{} {}"; // ex) $0 DESC

		public Order(String column, String order) {
			this.column = column;
			this.order = order;
		}

		String getConditionString() {
			return MessageFormatter.arrayFormat(PQL_SORT_ORDER, new String[]{column, order}).getMessage();
		}
	}
}