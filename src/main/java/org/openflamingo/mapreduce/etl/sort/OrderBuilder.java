package org.openflamingo.mapreduce.etl.sort;

import java.util.LinkedList;
import java.util.List;

/**
 * Sort의 Order By를 구성하는 빌더.
 *
 * @author Edward KIM
 * @author Seo Ji Hye
 * @since 0.1
 */
public class OrderBuilder {

	/**
	 * 하나 이상의 Order By를 유지하기 위한 리스트
	 */
	private List<SortDriver.Order> orders = new LinkedList<SortDriver.Order>();

	/**
	 * Order를 추가한다.
	 *
	 * @param order Sort의 Order By
	 */
	public void addOrder(SortDriver.Order order) {
		orders.add(order);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (SortDriver.Order order : orders) {
			String condition = order.getConditionString();
			builder.append(condition).append(",");
		}
		String valueString = builder.toString();
		return valueString.substring(0, valueString.length() - 1);
	}
}
