/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.openflamingo.mapreduce.etl.percentile;

import com.google.common.primitives.Doubles;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.openflamingo.mapreduce.core.Delimiter;
import org.openflamingo.mapreduce.util.CounterUtils;
import org.openflamingo.mapreduce.util.MathUtils;
import org.openflamingo.mapreduce.util.StringUtils;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * 지정한 키로 Group By하여 하나의 로우로 키와 값을 취합하는 Transpose ETL 리듀서
 *
 * @author Edward KIM
 * @since 0.1
 */
public class PercentileReducer extends Reducer<Text, Text, NullWritable, Text> {

    /**
     * Key와 Value의 구분자.
     */
    private String keyValueDelimiter;

    /**
     * Value를 구성하는 값들의 구분자.
     */
    private String valueDelimiter;

    /**
     * Quantiles 목록.
     */
    private double[] quantiles;

    /**
     * 출력 Value Text.
     */
    private Text valueText = new Text();

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Configuration configuration = context.getConfiguration();
        keyValueDelimiter = configuration.get("keyValueDelimiter", Delimiter.TAB.getDelimiter());
        valueDelimiter = configuration.get("valueDelimiter", Delimiter.COMMA.getDelimiter());

        String quantilesString = configuration.get("quantiles", "");
        if ("".equals(quantilesString)) {
            throw new IllegalArgumentException("You must specify 'quantiles' for Quantiles");
        }

        String[] strings = StringUtils.splitPreserveAllTokens(quantilesString, Delimiter.COMMA.getDelimiter());
        quantiles = new double[strings.length];
        for (int i = 0; i < strings.length; i++) {
            quantiles[i] = Double.parseDouble(strings[i]);
        }
    }

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        CounterUtils.writerReducerCounter(this, "ITEMS", context);

        List<Double> doubleValues = new LinkedList<Double>();
        Iterator<Text> iterator = values.iterator();
        while (iterator.hasNext()) {
            Text v = iterator.next();
            doubleValues.add(Double.parseDouble(v.toString()));
        }

        String percentile = MathUtils.percentile(Doubles.toArray(doubleValues), quantiles, valueDelimiter);
        valueText.set(key.toString() + keyValueDelimiter + percentile);
        context.write(NullWritable.get(), valueText);
    }
}