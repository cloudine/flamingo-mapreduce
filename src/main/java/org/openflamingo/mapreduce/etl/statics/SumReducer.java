package org.openflamingo.mapreduce.etl.statics;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.openflamingo.mapreduce.core.Delimiter;
import org.openflamingo.mapreduce.etl.statics.statics.StaticsModule;
import org.openflamingo.mapreduce.etl.statics.statics.SumModule;
import org.openflamingo.mapreduce.parser.CsvRowParser;
import org.openflamingo.mapreduce.util.StringUtils;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: ell
 * Date: 12. 1. 24
 * Time: 오후 10:51
 * To change this template use File | Settings | File Templates.
 */
public class SumReducer extends Reducer<NullWritable, Text, NullWritable, Text> {

    /**
     * 출력 파일의 구분자.
     */
    private String delimiter;

    private int columnLength;

    private String[] dataTypes;
    private StaticsModule[] modules;


    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

        Configuration configuration = context.getConfiguration();
        delimiter = configuration.get("outputDelimiter", Delimiter.COMMA.getDelimiter());
        dataTypes = StringUtils.commaDelimitedListToStringArray(configuration.get("dataTypes"));
        columnLength = configuration.getInt("columnsToStaticsLength", -1);

        // FIXME move to somewhere
        String[] staticsModes = StringUtils.commaDelimitedListToStringArray(configuration.get("staticsModes"));
        if (staticsModes.length != columnLength &&
                dataTypes.length != columnLength) {
            throw new IllegalArgumentException("Invalid Parameter Length");
        }

        modules = new SumModule[columnLength];
        for (int i = 0; i < columnLength; i++) {
            modules[i] = new SumModule(dataTypes[i]);
            //modules[i].setDataType(dataTypes[i]);
        }
    }

    @Override
    protected void reduce(NullWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        CsvRowParser parser = new CsvRowParser();
        parser.setInputDelimiter(delimiter);

        for (Text value : values) {
            parser.parse(value.toString());
            for (int i = 0; i < columnLength; i++) {
                modules[i].calculate(parser.get(i));
            }
        }

        // 합한 값 쓰기5
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < columnLength; i++) {
            builder.append(modules[i].toString()).append(delimiter);
        }

        String stringVaule = builder.substring(0, builder.length() - delimiter.length());
        context.write(key, new Text(stringVaule));
    }
}
