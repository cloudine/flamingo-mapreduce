package org.openflamingo.mapreduce.etl.statics;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.openflamingo.mapreduce.core.Delimiter;
import org.openflamingo.mapreduce.etl.statics.statics.Statics;
import org.openflamingo.mapreduce.etl.statics.statics.StaticsModule;
import org.openflamingo.mapreduce.etl.statics.statics.StaticsModuleRegistry;
import org.openflamingo.mapreduce.parser.CsvRowParser;
import org.openflamingo.mapreduce.util.StringUtils;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: ell
 * Date: 12. 1. 24
 * Time: 오후 10:51
 * To change this template use File | Settings | File Templates.
 */
public class StaticsReducer extends Reducer<NullWritable, Text, NullWritable, Text> {

	/**
	 * 출력 파일의 구분자.
	 */
	private String delimiter;

	private int columnLength;

	private String[] dataTypes;

	private StaticsModule[] modules;

	private long lineCount;


	@Override
	protected void setup(Context context) throws IOException, InterruptedException {
		Configuration configuration = context.getConfiguration();
		delimiter = configuration.get("outputDelimiter", Delimiter.COMMA.getDelimiter());
		columnLength = configuration.getInt("columnsToStaticsLength", -1);
		lineCount = configuration.getLong("lineCount", -1);
		dataTypes = StringUtils.commaDelimitedListToStringArray(configuration.get("dataTypes"));
		String[] staticsModes = StringUtils.commaDelimitedListToStringArray(configuration.get("staticsModes"));

		if (staticsModes.length != columnLength &&
			dataTypes.length != columnLength) {
			throw new IllegalArgumentException("Invalid Parameter Length");
		}

		modules = new StaticsModule[columnLength];
		for (int i = 0; i < columnLength; i++) {
			modules[i] = StaticsModuleRegistry.getModule(Statics.valueOf(staticsModes[i]));
			modules[i].setDataType(dataTypes[i]);
			modules[i].setCount(lineCount);
		}
	}

	@Override
	protected void reduce(NullWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		// FIXME messy over all
		CsvRowParser parser = new CsvRowParser();
		parser.setInputDelimiter(delimiter);

		for (Text value : values) {
			parser.parse(value.toString());
			for (int i = 0; i < columnLength; i++) {
				// TODO SUM ETL에서 계산한 값을 최종적으로 더해줌
				modules[i].calculate(parser.get(i));
			}
		}

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < columnLength; i++) {
			builder.append(modules[i].toString()).append(delimiter);
		}
		String result = builder.substring(0, builder.length() - delimiter.length());
		context.write(NullWritable.get(), new Text(result));
	}
}
