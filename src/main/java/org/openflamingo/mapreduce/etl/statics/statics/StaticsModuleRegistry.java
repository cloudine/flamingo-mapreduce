package org.openflamingo.mapreduce.etl.statics.statics;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: ell
 * Date: 12. 1. 27
 * Time: 오전 9:20
 * To change this template use File | Settings | File Templates.
 */
public class StaticsModuleRegistry {

	/**
	 *
	 * @param name
	 * @return Statics Module
	 */
	public static StaticsModule getModule(Statics name) {
		switch (name) {
			case AVERAGE:
				return new AverageModule();
			case MIN:
				return new MinModule();
			case MAX:
				return new MaxModule();
			case SUM:
				return new SumModule();
			case DEVIATION:
				return new DeviationModule();
			case STANDARD_DEVIATION:
				return new StandardDeviationModule();
			case VARIANT:
				return new VariantModule();
		}
		return null;
	}

	/**
	 *
	 * @param name
	 * @return
	 */
	public static StaticsModule getModule(String name) {
		switch (Statics.valueOf(name)) {
			case AVERAGE:
				return new AverageModule();
			case MIN:
				return new MinModule();
			case MAX:
				return new MaxModule();
			case SUM:
				return new SumModule();
			case DEVIATION:
				return new DeviationModule();
			case STANDARD_DEVIATION:
				return new StandardDeviationModule();
			case VARIANT:
				return new VariantModule();
		}
		return null;
	}
}
