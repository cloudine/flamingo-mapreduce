package org.openflamingo.mapreduce.etl.statics.statics;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: ell
 * Date: 12. 1. 27
 * Time: 오전 9:20
 * To change this template use File | Settings | File Templates.
 */
public class DataTypeRegistry {
    /**
     * 필터의 이름과 필터의 구현체를 매핑하는 필터맵.
     */
    private static Map<String, Number> numberMap = new HashMap<String, Number>();

    static {
        numberMap.put("INT", new BigInteger("0", 10));
        numberMap.put("INTEGER", new BigInteger("0", 10));
        numberMap.put("LONG", new BigInteger("0", 10));
        numberMap.put("FLOAT", new BigDecimal("0"));
        numberMap.put("DOUBLE", new BigDecimal("0"));
    }

    /**
     * 필터의 이름과 필터의 구현체를 매핑하는 필터맵.
     */
    private static Map<String, Number> maxNumberMap = new HashMap<String, Number>();

    static {
        maxNumberMap.put("INT", Long.MAX_VALUE);
        maxNumberMap.put("INTEGER", Long.MAX_VALUE);
        maxNumberMap.put("LONG", Long.MAX_VALUE);
        maxNumberMap.put("FLOAT", Float.MAX_VALUE);
        maxNumberMap.put("DOUBLE", Double.MAX_VALUE);
    }

    /**
     * 필터의 이름과 필터의 구현체를 매핑하는 필터맵.
     */
    private static Map<String, Number> minNumberMap = new HashMap<String, Number>();

    static {
        maxNumberMap.put("INT", Long.MIN_VALUE);
        maxNumberMap.put("INTEGER", Long.MIN_VALUE);
        maxNumberMap.put("LONG", Long.MIN_VALUE);
        maxNumberMap.put("FLOAT", Float.MIN_VALUE);
        maxNumberMap.put("DOUBLE", Double.MIN_VALUE);
    }

    /**
     * 지정한 이름의 Number 타입의 객체를 반환한다.
     *
     * @param name 타입명
     * @return 타입 구현체
     */
    public static Number get(String name) {
        return numberMap.get(name.toUpperCase());
    }

    public static Number getMaxNumber(String name) {
        return maxNumberMap.get(name.toUpperCase());
    }

    public static Number getMinNumber(String name) {
        return minNumberMap.get(name.toUpperCase());
    }
}
