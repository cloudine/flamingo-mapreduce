package org.openflamingo.mapreduce.etl.statics.statics;

/**
 * Created by IntelliJ IDEA.
 * User: ell
 * Date: 12. 1. 27
 * Time: 오전 3:05
 * To change this template use File | Settings | File Templates.
 */
public class MinMaxValueSelector {
    private String dataType;
    private Number result;

    public void init(String dataType) {
        this.dataType = dataType;
        setResultType();
    }

    private void setResultType() {
        if ("int".equalsIgnoreCase(dataType) || "long".equalsIgnoreCase(dataType)) {
            result = new Long(0);
        } else if ("float".equalsIgnoreCase(dataType)) {
            result = new Float(0);
        } else if ("double".equalsIgnoreCase(dataType)) {
            result = new Double(0);
        }
    }

    public void sumStringValueToStaticsTypes(String value) {

        if ("int".equalsIgnoreCase(dataType) || "long".equalsIgnoreCase(dataType)) {
            //long temp = (Long)results.get(index) + Long.parseLong(value);
            result = result.longValue() + Long.parseLong(value);
        } else if ("float".equalsIgnoreCase(dataType)) {
            result = result.floatValue() + Float.parseFloat(value);
        } else if ("double".equalsIgnoreCase(dataType)) {
            result = result.doubleValue() + Double.parseDouble(value);
        }
    }
}
