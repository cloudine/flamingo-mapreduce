package org.openflamingo.mapreduce.etl.statics;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.openflamingo.mapreduce.core.Delimiter;
import org.openflamingo.mapreduce.parser.CsvRowParser;
import org.openflamingo.mapreduce.util.ArrayUtils;
import org.openflamingo.mapreduce.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: ell
 * Date: 12. 1. 24
 * Time: 오후 10:50
 * To change this template use File | Settings | File Templates.
 */
public class SumMapper extends Mapper<LongWritable, Text, NullWritable, Text> {

    /**
     * SLF4J Logging
     */
    private static Logger logger = LoggerFactory.getLogger(SumMapper.class);

    /**
     * 입력 파일의 구분자.
     */
    private String inputDelimiter;

    private String outputDelimiter;

    /**
     * 통계치를 계산할 컬럼들의 인덱스 배열.
     */
    private Integer[] columnsToStatics;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {

        Configuration configuration = context.getConfiguration();
        inputDelimiter = configuration.get("inputDelimiter", Delimiter.COMMA.getDelimiter());
        outputDelimiter = configuration.get("outputDelimiter", Delimiter.COMMA.getDelimiter());

        String[] stringColumns = StringUtils.commaDelimitedListToStringArray(configuration.get("columnsToStatics"));

        if (stringColumns.length == 0) {
            throw new IllegalArgumentException("Invalid Parameter Length");
        }

        // TODO length check?

        columnsToStatics = ArrayUtils.toIntegerArray(stringColumns);
        configuration.set("columnsToStaticsLength", Integer.toString(columnsToStatics.length));
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        CsvRowParser parser = new CsvRowParser();
        parser.setInputDelimiter(inputDelimiter);
        parser.parse(value.toString());

        StringBuilder builder = new StringBuilder();
        for (int index : columnsToStatics) {
            builder.append(parser.get(index)).append(outputDelimiter);
        }

        String valueString = builder.substring(0, builder.length() - outputDelimiter.length());
        context.write(NullWritable.get(), new Text(valueString));
    }
}
