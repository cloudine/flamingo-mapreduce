package org.openflamingo.mapreduce.etl.statics.statics;

import org.openflamingo.mapreduce.type.DataType;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by IntelliJ IDEA.
 * User: ell
 * Date: 12. 1. 25
 * Time: 오전 5:18
 * To change this template use File | Settings | File Templates.
 */
public class SumModule extends StaticsModule {

    public SumModule() {
        super();
    }

    public SumModule(String dataType) {
        super(dataType);
    }

    @Override
    public void calculate(String s) {
        if (dataType.equals(DataType.INTEGER.getDataType()) ||
                dataType.equals(DataType.LONG.getDataType())) {
            value = ((BigInteger) value).add(new BigInteger(s, radix));
        } else if (dataType.equals(DataType.FLOAT.getDataType()) ||
                dataType.equals(DataType.DOUBLE.getDataType())) {
            value = ((BigDecimal) value).add(new BigDecimal(s));
        }
    }
}
