package org.openflamingo.mapreduce.etl.statics.statics;

/**
 * Created by IntelliJ IDEA.
 * User: ell
 * Date: 12. 2. 4
 * Time: 오전 2:30
 * To change this template use File | Settings | File Templates.
 */
public enum Statics {

    MIN("min"),
    MAX("max"),
    SUM("sum"),
    AVERAGE("average"),
    DEVIATION("deviation"),
    STANDARD_DEVIATION("standard_deviation"),
    VARIANT("variant");

    /**
     * Statics 모듈의 종류 이름
     */
    private String staticsType;

    /**
     * @param typeName
     */
    Statics(String typeName) {
        staticsType = typeName.toLowerCase();
    }

    String getStatics() {
        return staticsType;
    }
}
