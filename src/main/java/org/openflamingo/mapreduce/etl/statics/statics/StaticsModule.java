package org.openflamingo.mapreduce.etl.statics.statics;

import org.openflamingo.mapreduce.type.DataType;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by IntelliJ IDEA.
 * User: ell
 * Date: 12. 1. 25
 * Time: 오전 6:55
 * To change this template use File | Settings | File Templates.
 */
public abstract class StaticsModule {

    protected Number value;

    protected String dataType;

    protected int radix = 10;

    protected long count;

    protected StaticsModule() {
    }

    protected StaticsModule(String dataType) {
        this.dataType = dataType;
        value = DataTypeRegistry.get(dataType);
    }

    protected StaticsModule(String dataType, long count) {
        this.count = count;
        this.dataType = dataType;
        value = DataTypeRegistry.get(dataType);
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
        value = DataTypeRegistry.get(dataType);
    }

    public void setCount(long count) {
        this.count = count;
    }

    protected void setValueToMinimumNumber() {
        value = DataTypeRegistry.getMinNumber(dataType);
    }

    protected void setValueToMaximumNumber() {
        value = DataTypeRegistry.getMaxNumber(dataType);
    }

    public void calculate(String s) {
    }

    public Number getResult() {
        // FIXME casting is issue
        return value;
    }

    public String toString() {
        if (dataType.equals(DataType.INTEGER.getDataType()) || dataType.equals(DataType.LONG.getDataType())) {
            return ((BigInteger) value).toString(radix);
        } else if (dataType.equals(DataType.FLOAT.getDataType()) || dataType.equals(DataType.DOUBLE.getDataType())) {
            return ((BigDecimal) value).toPlainString();
        } else {
            // if data type error.
            return null;
        }
    }
}
