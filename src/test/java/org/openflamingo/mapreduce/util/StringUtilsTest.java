package org.openflamingo.mapreduce.util;

import junit.framework.Assert;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

/**
 * String Utility Unit Test Case.
 *
 * @author Edward KIM
 * @since 0.1
 */
public class StringUtilsTest {

    @Test
    public void commaDelimitedListToStringArray() {
        String[] strings = StringUtils.commaDelimitedListToStringArray("0");
        Assert.assertEquals(1, strings.length);
        Assert.assertEquals("0", strings[0]);

        strings = StringUtils.commaDelimitedListToStringArray("0,1,2,");
        Assert.assertEquals(4, strings.length);
        Assert.assertEquals("0", strings[0]);
        Assert.assertEquals("1", strings[1]);
        Assert.assertEquals("2", strings[2]);
        Assert.assertEquals("", strings[3]);
    }

    @Test
    public void joinArray() {
        String input = "0,1,2,3,4";
        String[] strings = StringUtils.commaDelimitedListToStringArray(input);
        String join = StringUtils.join(strings, ",");

        Assert.assertEquals(input, join);
    }

    @Test
    public void joinMap() {
        Map<String, String> params = new TreeMap<java.lang.String, java.lang.String>();
        params.put("A", "B");
        params.put("C", "D");
        params.put("E", "F");
        String join = StringUtils.join(params, "=", "-D", "", " ", "\"", "\"");

        String expected = "-DA=\"B\" -DC=\"D\" -DE=\"F\"";
        Assert.assertEquals(expected, join);
    }
}
