package org.openflamingo.mapreduce.aggregator;

import org.junit.Assert;
import org.junit.Test;

/**
 * LongSumAggregator 단위 테스트 케이스.
 *
 * @author Edward KIM
 * @since 1.0
 */
public class LongSumAggregatorTest {

	@Test
	public void aggregate() {
		LongSumAggregator aggregator = new LongSumAggregator();
		aggregator.aggregate(1);
		aggregator.aggregate(2);
		aggregator.aggregate(3);

		Assert.assertEquals(6, aggregator.getAggregatedValue().get());
	}
}
