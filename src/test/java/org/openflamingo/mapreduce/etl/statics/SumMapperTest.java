package org.openflamingo.mapreduce.etl.statics;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

/**
 * Replace Column Mapper에 대한 단위 테스트 케이스.
 *
 * @author Jihye Seo
 * @since 0.1
 */
public class SumMapperTest {
    private Mapper mapper;
    private MapDriver driver;

    @Before
    public void setUp() {
        mapper = new SumMapper();
        driver = new MapDriver(mapper);
    }

    @Test
    public void test1() {
        Configuration conf = new Configuration();
        conf.set("columnsToStatics", "0");
        driver.setConfiguration(conf);

        driver.withInput(new LongWritable(1), new Text("a,b,c,d"));
        driver.withOutput(NullWritable.get(), new Text("a"));
        driver.runTest();
    }

    @Test
    public void test2() {
        Configuration conf = new Configuration();
        conf.set("columnsToStatics", "1,2,3");
        driver.setConfiguration(conf);

        driver.withInput(new LongWritable(1), new Text("a,b,c,d"));
        driver.withOutput(NullWritable.get(), new Text("b,c,d"));
        driver.runTest();
    }

    @Test
    public void test3() {
        Configuration conf = new Configuration();
        conf.set("inputDelimiter", "::");
        conf.set("columnsToStatics", "1,2,3");
        driver.setConfiguration(conf);

        driver.withInput(new LongWritable(1), new Text("a::b::c::d"));
        driver.withOutput(NullWritable.get(), new Text("b,c,d"));
        driver.runTest();
    }

    @Test
    public void test4() {
        Configuration conf = new Configuration();
        conf.set("inputDelimiter", ",");
        conf.set("outputDelimiter", "::");
        conf.set("columnsToStatics", "1,2,3");
        driver.setConfiguration(conf);

        driver.withInput(new LongWritable(1), new Text("a,b,c,d"));
        driver.withOutput(NullWritable.get(), new Text("b::c::d"));
        driver.runTest();
    }

    @Test(expected = IllegalArgumentException.class)
    public void test5() {
        Configuration conf = new Configuration();
        conf.set("columnsToStatics", "");
        driver.setConfiguration(conf);

        driver.withInput(new LongWritable(1), new Text("a,b,c,d"));
        driver.runTest();
    }

    @Test(expected = RuntimeException.class)
       public void test6() {
           Configuration conf = new Configuration();
           conf.set("columnsToStatics", "1");
           driver.setConfiguration(conf);

           driver.withInput(new LongWritable(1), new Text("a,b,c,d"));
           driver.withInput(NullWritable.get(), new Text("a,b,c,d"));
           driver.runTest();
       }
}
