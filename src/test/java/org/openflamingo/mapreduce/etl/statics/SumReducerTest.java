package org.openflamingo.mapreduce.etl.statics;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Replace Column Mapper에 대한 단위 테스트 케이스.
 *
 * @author Jihye Seo
 * @since 0.1
 */
public class SumReducerTest {
    private Reducer reducer;
    private ReduceDriver driver;

    @Before
    public void setUp() {
        reducer = new SumReducer();
        driver = new ReduceDriver(reducer);
    }

    @Test
    public void test1() {
        Configuration conf = new Configuration();
        conf.set("columnsToStaticsLength", "4");
        conf.set("dataTypes", "int,long,double,float");
        conf.set("staticsModes", "sum,sum,sum,sum");

        driver.setConfiguration(conf);

        List<Text> list = new ArrayList<Text>();
        list.add(new Text("1,1,1.0,1.0"));
        list.add(new Text("2,2,2.0,2.0"));
        list.add(new Text("3,3,3.0,3.0"));
        driver.withInput(NullWritable.get(), list);
        driver.withOutput(NullWritable.get(), new Text("6,6,6.0,6.0"));
        driver.runTest();
    }
}
