package org.openflamingo.mapreduce.etl.statics;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.junit.Before;
import org.junit.Test;

/**
 * Replace Column Mapper에 대한 단위 테스트 케이스.
 *
 * @author Jihye Seo
 * @since 0.1
 */
public class StaticsMapperReducerTest {
    private Mapper mapper;
    private Reducer reducer;
    private MapReduceDriver driver;

    @Before
    public void setUp() {
        mapper = new StaticsMapper();
        reducer = new StaticsReducer();
        driver = new MapReduceDriver(mapper, reducer);
    }

    @Test
    public void test1() {
        Configuration conf = new Configuration();
        conf.set("columnsToStatics", "0");
        conf.set("columnsToStaticsLength", "1");
        conf.set("dataTypes", "int");
        conf.set("staticsModes", "SUM");
        driver.setConfiguration(conf);

        driver.withInput(new LongWritable(1), new Text("1"));
        driver.withInput(new LongWritable(2), new Text("2"));
        driver.withInput(new LongWritable(3), new Text("3"));
        driver.withOutput(NullWritable.get(), new Text("6"));
        driver.runTest();
    }

    @Test
    public void test2() {
        Configuration conf = new Configuration();
        conf.set("columnsToStatics", "0,1");
        conf.set("columnsToStaticsLength", "2");
        conf.set("dataTypes", "long,double");
        conf.set("staticsModes", "SUM,SUM");
        driver.setConfiguration(conf);

        driver.withInput(new LongWritable(1), new Text("1,1.0"));
        driver.withInput(new LongWritable(2), new Text("2,2.0"));
        driver.withInput(new LongWritable(3), new Text("3,3.0"));
        driver.withOutput(NullWritable.get(), new Text("6,6.0"));
        driver.runTest();
    }

    @Test(expected = IllegalArgumentException.class)
    public void test4() {
        Configuration conf = new Configuration();
        conf.set("columnsToStatics", "1,2");
        conf.set("staticsModes", "MAX");
        driver.setConfiguration(conf);

        driver.withInput(new LongWritable(1), new Text("1,2,3,4"));
        driver.runTest();
    }

}
