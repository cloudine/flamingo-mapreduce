package org.openflamingo.mapreduce.etl.sort;

import junit.framework.Assert;
import org.junit.Test;

/**
 * OrderBuilder Unit Test.
 *
 * @author Edward KIM
 * @since 0.1
 */
public class OrderBuilderTest {

    @Test
    public void addOrderAndToString1() {
        OrderBuilder builder = new OrderBuilder();
        builder.addOrder(new SortDriver.Order("$0", "ASC"));
        builder.addOrder(new SortDriver.Order("$1", "DESC"));
        builder.addOrder(new SortDriver.Order("$5", "ASC"));
        Assert.assertEquals("$0 ASC,$1 DESC,$5 ASC", builder.toString());
    }

    @Test
    public void addOrderAndToString2() {
        OrderBuilder builder = new OrderBuilder();
        builder.addOrder(new SortDriver.Order("$0", "ASC"));
        builder.addOrder(new SortDriver.Order("$1", "DESC"));
        Assert.assertEquals("$0 ASC,$1 DESC", builder.toString());
    }

    @Test
    public void addOrderAndToString3() {
        OrderBuilder builder = new OrderBuilder();
        builder.addOrder(new SortDriver.Order("$0", "ASC"));
        Assert.assertEquals("$0 ASC", builder.toString());
    }
}
